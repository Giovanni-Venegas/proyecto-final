# Proyecto-final
Este repositorio es el proyecto final de la materia de Seguridad e Integridad de la Información de la Universidad Iberoamericana Ciudad de México, esta aplicación móvil desarrollada para iOS, tiene como objetivo aplicar conceptos de seguridad en la misma en un entorno de Hospital. 

Levantamiento de los servicios: 

- > git@gitlab.com:Giovanni-Venegas/proyecto-final.git
- > https://gitlab.com/Giovanni-Venegas/proyecto-final.git


Enlace a la documentación: proyecto-final.git/Proyecto_Final_-_Ciberseguridad.pdf
